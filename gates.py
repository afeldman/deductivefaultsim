"""
Gates for simulator
"""

class Gate(object):
	_instantiated_gates = []
	def __init__(self,output_net,input_netlist):
		Gate._instantiated_gates.append(self)
		self.input_netlist = input_netlist
		self.output_net = output_net
		#fault list is a list of tuples (net,stuck@val)
		self.fault_list = set([])

	@staticmethod
	def clean_all():
		for gate in Gate._instantiated_gates:
			gate.fault_list = set([])
		
	def __str__(self):
		return str(self.__class__) + " " + str(self.input_netlist) + " " + str(self.output_net)
		
	def __repr__(self):
		return self.__str__()
		
	def is_able_evaluate(self):
		for input_net in self.input_netlist:
			if input_net.value ==None:
				return False
		return True
		
	def evaluate(self):
		raise NotImplemented("evaluate not implemented")
				
class GateAnd(Gate):
	def __init__(self,output,input_netlist):
		self.controlling_value = False;
		super(GateAnd,self).__init__(output,input_netlist)
	def evaluate(self):
		val_out = True
		#controlling_nets = []
		for net in self.input_netlist:
		#	if (net.value==0):
		#		controlling_nets.append(net)
			val_out &= (net.value)
		
		self.output_net.value = val_out
		
		#returns a list of controlling nets
		#return controlling_nets
		
class GateOr(Gate):
	def __init__(self,output,input_netlist):
		self.controlling_value = True;
		super(GateOr,self).__init__(output,input_netlist)
	def evaluate(self):
		val_out = False
		#controlling_nets = []
		for net in self.input_netlist:
			#if (net.value==1):
			#	controlling_nets.append(net)
			val_out |= net.value

		self.output_net.value = val_out

		#returns a list of controlling nets
		#return controlling_nets
		
class GateNand(Gate):
	def __init__(self,output,input_netlist):
		self.controlling_value = False;
		super(GateNand,self).__init__(output,input_netlist)
	def evaluate(self):
		val_out = True
		#controlling_nets = []
		for net in self.input_netlist:
			#if (net.value==0):
			#	controlling_nets.append(net)
			val_out &= net.value
			
		val_out = not(val_out)

		self.output_net.value = val_out

		#returns a list of controlling nets
		#return controlling_nets
		
class GateNor(Gate):
	def __init__(self,output,input_netlist):
		self.controlling_value = True;
		super(GateNor,self).__init__(output,input_netlist)
	def evaluate(self):
		val_out = False
		#controlling_nets = []
		for net in self.input_netlist:
			#if (net.value==1):
			#	controlling_nets.append(net)
			val_out |= net.value
			
		val_out = not(val_out)

		self.output_net.value = val_out

		#returns a list of controlling nets
		#return controlling_nets
		
class GateBuffer(Gate):
	def __init__(self,output,input_netlist):
		self.controlling_value = None
		if len(input_netlist)>1:
			raise ValueError("buffer input netlist must be of length 1")
		super(GateBuffer,self).__init__(output,input_netlist)
	
	def evaluate(self):
		val_out = self.input_netlist[0].value
		
		self.output_net.value = val_out

		#returns a list of controlling nets
		#return self.input_netlist
		
class GateNot(Gate):
	def __init__(self,output,input_netlist):
		self.controlling_value = None
		if len(input_netlist)>1:
			raise ValueError("not gate input netlist must be of length 1")
		super(GateNot,self).__init__(output,input_netlist)

	def evaluate(self):
		val_out = not(self.input_netlist[0].value)

		self.output_net.value = val_out

		#returns a list of controlling nets
		#return self.input_netlist
