"""
input and output functions
"""

from net import *
from gates import *

def gen_input_file(filename,vecs,input_nets):
	input_net_header = ','.join([str(x.net_number) for x in input_nets])
	with open(filename,'w') as f:
		f.write(input_net_header+"\n")
		for vec in vecs:
			f.write(','.join([str(int(x)) for x in vec]) + '\n')

def read_netlist(filename):
	net_map = {}
	gate_map={}
	with open(filename,'r') as f:
		for line in f:
			line = line.replace("\r","")
			line = line.replace("\n","")
			line = line.replace("  "," ")
			if line.replace(" ","")=="":
				continue
			params = line.split(' ')
			gate_type = params[0]
			param_list = []
			for net in params[1:]:
				net = int(net)
				if (net!=(-1)):
					if net not in net_map:
						net_map[net] = Net(net)
					param_list.append(net_map[net])
			
			out = param_list[-1]
			input_params = param_list[0:-1]
			
			gate = None
			
			if gate_type == "AND":
				gate = GateAnd(out,input_params)
			elif gate_type == "BUF":
				gate = GateBuffer(out,input_params)
			elif gate_type == "OR":
				gate = GateOr(out,input_params)
			elif gate_type == "NAND":
				gate = GateNand(out,input_params)
			elif gate_type == "NOR":
				gate = GateNor(out,input_params)
			elif gate_type == "INV":
				gate = GateNot(out,input_params)
			elif gate_type == "INPUT":
				input_nets = param_list
			elif gate_type == "OUTPUT":
				output_nets = param_list
			else:
				print "unrecognized gate type"
			
			if gate != None:	
				gate_map[out] = gate
	input_nets_sorted = sorted(input_nets,key = lambda input_net: input_net.net_number)
	return (input_nets_sorted,output_nets,gate_map)
	
def read_input(filename,input_nets):
	test_vectors = []
	with open(filename,'r') as f:
		header = f.readline()
		file_net_numbers = header.split(",")
		for (input_net,file_net_number) in zip(input_nets,file_net_numbers):
			file_net_number = int(file_net_number)
			if input_net.net_number!=file_net_number:
				raise ValueError("input nets in netlist does not match input nets in input vector file")
		for line in f:
			line = line.replace("\r","")
			line = line.replace("\n","")
			line = line.replace("  "," ")
			if line.replace(" ","")=="":
				continue
			values = line.split(",")
			converted_values = []
			for value in values:
				converted_values.append(value=='1')
			test_vectors.append(converted_values)
	return test_vectors


def read_fault_file(filename,input_netlist):
	out={}
	temp_map = {}
	for net in input_netlist:
		out[net] = []
		temp_map[net.net_number] = net
	with open(filename,'r') as f:
		f.readline() #garbage line
		for line in f:
			line.strip()
			line = line.replace("\r","")
			line = line.replace("\n","")
			net_fault = line.split(' ')
			netnum = int(net_fault[0])
			s_a_value = net_fault[1]=='1'
			if s_a_value not in out[temp_map[netnum]]:
				out[temp_map[netnum]].append(s_a_value)
	return out

def write_to_csv(filename,out,output_netlist):
	netlist_nums = [str(net.net_number) for net in output_netlist]
	with open(filename,'w') as f:
		f.write(",".join(netlist_nums))
		f.write("\n")
		out_list=[]
		for line in out:
			out_list.append(",".join(line))
		out_str="\n".join(out_list)
		f.write(out_str)

def write_detected_faults(filename,detected_faults):
	with open(filename,'w') as f:
		f.write("Netno stuck-at-value\n")
		for fault in detected_faults:
			f.write("%d %d\n" % (fault[0].net_number,fault[1]))
