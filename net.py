"""
Net declaration
"""

class Net(object):
	_instantiated_nets = []
	def __init__(self,net_number):
		self.net_number = net_number
		self.value = None
		Net._instantiated_nets.append(self)
		
	def __str__(self):
		return "NET:" + str(self.net_number)
		
	def __repr__(self):
		return self.__str__()
		
	@staticmethod
	def clean_all():
		for net in Net._instantiated_nets:
			net.value = None
	
	@staticmethod
	def total_nets():
		return len(Net._instantiated_nets)