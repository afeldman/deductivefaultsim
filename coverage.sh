#!/bin/bash

files=$(ls ./examples|egrep "s[a-z0-9]+_?[0-9].txt")

for file in $files
do
	file="examples/"${file}
	inputs=(`grep INPUT $file| tr " " "\n"`)
	prefix=${file%".txt"}
	prefix=${prefix%" "}
	num_arr=${#inputs[@]}
	num_inputs=$((num_arr-2))
	coverage_vecs_max=$((2**$num_inputs))
	output_file="${prefix}_coverage.csv"
	input_file="${prefix}_input.csv"
	if [ ! -f $input_file ];
	then
		echo "No input file for ${prefix}"
		continue
	fi
	if [ -f $output_file ];
	then
		rm $output_file
	fi
	for ((c=1;c<=$coverage_vecs_max && c<=100;c++));
	do
		python simulator.py -n -c $c $prefix >> $output_file
	done
done
