#!/usr/bin/python2.7

import random
import sys

from gates import *
from net import *
from file import *	

def simulate(input_nets,output_nets,gate_map,test_vectors,file_fault_list=None):
	"""	Completes a deductive fault simulation on the circuit defined by a netlist for the faults in file_fault_list.
	If no fault list is given, by default, all possible input faults are simulated.

	Keyword Arguments:
	input_nets -- a list of Net objects representing the inputs to the circuit
	output_nets -- a list of Net objects representing the outputs of the circuit
	gate_map -- a dictionary object representing the gates that drive a net (key is a net object, value is the gate object)
	test_vectors -- a list of test vectors (true/false lists) to test the circuit with.
	file_fault_list -- a dictionary of lists of faults that are allowed in the simulation (keyed by a Net object)
	
	Returns (2 dimensional tuple):
	ind[0]-> a list of detected faults
	ind[1]-> the output values for each input test vector
	"""
	def eval_gate(gate):
		"""	Recursive backwards traversal of a circuit path from a gate to an input for evaluation of the output of that gate.
		Builds the fault lists for deductive fault simulation in place during the traversal.

		Keyword Arguments:
		gate -- a Gate object representing the gate to evaluate the output
		"""
		#initially, there are no controlling net faults on the gate
		controlling_net_faults = None

		#for each gate input, if the input net's value is unavailable, we need to evaluate the gate
		#that drives that net
		for input_net in gate.input_netlist:
			if input_net.value==None:
				eval_gate(gate_map[input_net])
			
			#value is available for an input net, is it controlling?
			if gate.controlling_value==None or input_net.value==gate.controlling_value:
				if input_net in gate_map:
					if controlling_net_faults.__class__==set:
						controlling_net_faults &= gate_map[input_net].fault_list
					else:
						controlling_net_faults = gate_map[input_net].fault_list.copy()
				else:
					if file_fault_list:
						temp_set = set([])
						for s_a_value in file_fault_list[input_net]:
							temp_set.add((input_net,s_a_value))
						if controlling_net_faults.__class__==set:
							controlling_net_faults &= temp_set
						else:
							controlling_net_faults = temp_set
					else:
						if controlling_net_faults.__class__==set:
							controlling_net_faults &= set((input_net,not(input_net.value)))
						else:
							temp_set=set([])
							temp_set.add((input_net,not(input_net.value)))
							controlling_net_faults = temp_set
			#it isn't controlling
			else:
				if input_net in gate_map:
					gate.fault_list |= gate_map[input_net].fault_list
				else:
					if file_fault_list:
						temp_set = set([])
						for s_a_value in file_fault_list[input_net]:
							temp_set.add((input_net,s_a_value))
						gate.fault_list |= temp_set
					else:
						#reached actual input
						gate.fault_list.add((input_net,not(input_net.value)))
		gate.evaluate()
		if controlling_net_faults.__class__==set:
			gate.fault_list = (controlling_net_faults - gate.fault_list)
			gate.fault_list.add((gate.output_net,not(gate.output_net.value)))
		else:
			gate.fault_list.add((gate.output_net,not(gate.output_net.value)))
		return

	ret_list = []
	detected_fault_set = set([])
	for test_vector in test_vectors:
		out_list = []
		
		for output_net in output_nets:
			#set elements for test
			for (input_net,test_element) in zip(input_nets,test_vector):
				input_net.value = test_element
			eval_gate(gate_map[output_net])
			detected_fault_set |= gate_map[output_net].fault_list
			if output_net.value:
				out_list.append('1')
			else:
				out_list.append('0')

			#clear net values
			Net.clean_all()

			#clear fault lists
			Gate.clean_all()

		#add results
		ret_list.append(out_list)
		
		
		
			
	return (list(detected_fault_set),ret_list)
	
from optparse import OptionParser

def main(options,args):
	"main function that calls the readers to parse input files, runs the fault sim, and writes output files."

	prefix = args[0]
	netlist_filename = prefix + ".txt"
	input_filename = prefix + "_input.csv"
	output_filename = prefix + "_output.csv"
	fault_filename = prefix + "_faults.txt"
		
	netlist = read_netlist(netlist_filename)
	input_netlist = netlist[0]
	output_netlist = netlist[1]
	gate_map=netlist[2]
	if options.coverage:
		if options.coverage>(2**len(input_netlist)):
			sys.stderr.write("requested more coverage vectors than the circuit can have - can't do it scotty!\n")
			exit()
		vecs = []
		num_max=(2**len(input_netlist))-1
		for vecnum in range(options.coverage):
			vector=[]
			testvec_num=random.randint(0,num_max)
			while testvec_num>0:
				vector.append(bool(testvec_num&1))
				testvec_num>>=1
			vector.extend([False]*(len(input_netlist)-len(vector)))
			vecs.append(vector)
	else:
		try:
			vecs = read_input(input_filename,input_netlist)
		except IOError:
			sys.exit("[ERROR] Unable to read input file "+input_filename)

	file_fault_list = None
	if options.fault_file:
		try:
			file_fault_list=read_fault_file(options.fault_file,input_netlist)
		except IOError:
			sys.exit("[ERROR] Unable to read fault file "+options.fault_file)

	(detected_faults,out) = simulate(input_netlist,output_netlist,gate_map,vecs,file_fault_list)
	if options.gen_output:
		write_to_csv(output_filename,out,output_netlist)
	if options.gen_fault:
		write_detected_faults(fault_filename,detected_faults)
	if options.coverage:
		cov_id=random.randint(0,2**15)
		if options.verbose:
			print "coverage id: "+str(cov_id)
		if options.gen_coverage_infile:
			cov_in_filename=prefix+"_coverage_input_"+str(cov_id)+".csv"
			gen_input_file(cov_in_filename,vecs,input_netlist)
		if options.verbose:
			print "test vectors:"
			for vec in vecs:
				print ''.join([str(int(x)) for x in vec])
			print "detected faults:%d" % len(detected_faults)
			print "total nets:%d     total possible faults:%d" % (Net.total_nets(), 2* Net.total_nets())
		else:
			vector_str_list=[]
			for vec in vecs:
				vector_str_list.append(''.join([str(int(x)) for x in vec]))
			print str(options.coverage) + "," + str(len(detected_faults)) + "," + str(2* Net.total_nets())
	elif options.verbose:
		print "detected faults:%d" % len(detected_faults)
		for fault in detected_faults:
			print fault
			
if __name__ == "__main__":
	usage = "%prog [options] prefix\nfiles are [prefix].txt [prefix]_input.csv [prefix]_output.csv [prefix]_faults.txt"
	parser = OptionParser(usage)
	parser.add_option("-c", "--coverage",type="int", dest="coverage", help="number of vectors to generate for coverage")
	parser.add_option("-i", "--covinputfile",action="store_true", dest="gen_coverage_infile", help="generate a coverage vector input file")
	parser.add_option("-n", "--nooutputfile",action="store_false", dest="gen_output", help="suppress output file generation")
	parser.add_option("-z", "--faultfilein", dest="fault_file", help="input fault file")
	parser.add_option("-f", "--faultfileout",action="store_true", dest="gen_fault", help="generate fault list file", default=False)
	parser.add_option("-v", "--verbose",action="store_true", dest="verbose", help="prints out verbose messages", default=False)
	
	(options,args)=parser.parse_args()
	if len(args)!=1:
		parser.print_help()
	else:
		main(options,args)
